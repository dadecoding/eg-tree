var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  res.render('index', {
    title: 'EG Assignment',
    description: 'Hope it works'
  });
});

module.exports = router;
