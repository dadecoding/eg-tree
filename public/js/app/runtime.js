requirejs.config({
	baseUrl: 'js/',
	paths: {
		jquery: 'libs/jquery.min',
		store: 'libs/store.min',

		Tree: 'app/modules/ui/Tree',
		TreeApp: 'app/modules/TreeApp',
		Node: 'app/modules/ui/Node',
		Storage: 'app/modules/utils/Storage',
		Controller: 'app/modules/utils/Controller',
	}
});

require(['TreeApp', 'Node'], function(TreeApp, Node) {
	// Base data set
	data=[{text:"Parent 1"},{text:"Parent 2",nodes:[{text:"Child 1",nodes:[{text:"Grandchild 1",nodes:[{text:"Inception 1"},{text:"Inception 2"}]},{text:"Grandchild 2",nodes:[{text:"Inception 1"},{text:"Inception 2"}]}]}]}];

	// Global for the sake of playing in console
	treeApp = new TreeApp(data);
});