data = [
	{
		text: 'Parent 1'
	},
	{
		text: 'Parent 2',
		nodes: [
		{
			text: 'Child 1',
			nodes: [
				{
					text: 'Grandchild 1',
					nodes: [
						{
							text: 'Inception 1'
						},
						{
							text: 'Inception 2'
						}
					]
				},
				{
					text: 'Grandchild 2',
					nodes: [
						{
							text: 'Inception 1'
						},
						{
							text: 'Inception 2'
						}
					]
				}
				]
			}
		]
	}
];