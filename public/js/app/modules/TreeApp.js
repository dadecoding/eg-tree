define(['jquery', 'Tree'], function($, Tree) {

	function TreeApp(data) {
		this.tree = {};
		this.nodes = [];

		if (this.nodes) {
			this.tree = $.extend(true, [], data);
			this.initialTree({ nodes: this.tree }, 0);
			new Tree(this.tree);
		}
	}

	TreeApp.prototype = {
		constructor: TreeApp,

		initialTree: function(node, level) {
			if (!node.nodes) return;

			var _this = this;
			var parent = node;
			
			level += 1;

			$.each(node.nodes, function(index, node) {
				node.nodeId = _this.nodes.length;
				node.parentId = parent.nodeId;
				_this.nodes.push(node);
				if (node.nodes) {
					_this.initialTree(node, level);
				}
			});
		}
	}

	return TreeApp;

});