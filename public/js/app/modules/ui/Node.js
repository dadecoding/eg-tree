define(['jquery', 'TreeApp'], function($, TreeApp) {

	function Node(nodeId, text, parentId, level, children) {
		this.nodeId = nodeId;
		this.text = text;
		this.parentId = parentId;
		this.level = level;
		this.element = {};
		this.children = children;

		this.renderNode(parentId);

	}

	Node.prototype = {
		constructor: Node,

		renderNode: function(parentId) {
			var padding = this.level * 25;
			var nodeEl = '<li class="list-group-item" data-id="'+this.nodeId+'" style="padding-left: '+padding+'px">';
			
			if (this.children) {
				nodeEl+= '<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>';
			}
			
			nodeEl+= '<span class="nodeText">'+this.text+'</span>';
			nodeEl+= '<span class="glyphicon glyphicon-remove pull-right" aria-hidden="true"></span>';
			nodeEl+= '<span class="glyphicon glyphicon-pencil pull-right" aria-hidden="true"></span>';
			nodeEl+= '<span class="glyphicon glyphicon-plus pull-right" aria-hidden="true"></span>';
			nodeEl+= '</li>';

			if (parentId > 0) {
				var parentElement = $('.list-group-item[data-id="'+parentId+'"]');
				parentElement.after(nodeEl);
				this.element = parentElement.next();
			} else {
				$('#tree').append(nodeEl);	
				this.element = $('#tree li:last-child');
			}
			
		},

		selectNode: function() {
			$('#tree li.active').removeClass('active');
			$('#element-title').val(this.text);
			this.element.addClass('active');
		},

		editNode: function() {
			

			var editMode = $(this.element).find('.nodeText.editing').length;

			if (editMode) {

				var value = $(this.element).find('input').val();
				this.text = value;
				$(this.element).find('.nodeText').html(value).removeClass('editing');

			} else {
				
				$('.nodeText.editing').each(function() {
					var value = $(this).find('input').attr('placeholder');
					$(this).html(value).removeClass('editing');
				});

				var text = $(this.element).find('.nodeText').text();
				$(this.element).find('.nodeText').addClass('editing').html('<input type="text" value="'+text+'">');
				$(this.element).find('.nodeText input').attr('placeholder', text);
			}
			
		},

		collapseNode: function(parent) {
			if (parent) {
				$(this.element).find('.glyphicon-chevron-down').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-right');
			} else {
				$(this.element).slideUp(100);
				$(this.element).find('.glyphicon-chevron-down');
			}
		},

		expandNode: function(parent) {
			if (parent) {
				$(this.element).find('.glyphicon-chevron-right').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down');
			} else {
				$(this.element).find('.glyphicon-chevron-right').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down');
				$(this.element).slideDown(100);
			}
		},

		removeNode: function() {
			$(this.element).slideUp(100, function() { $(this).remove(); });
		}

	};



	return Node;

});