define(['jquery', 'Node'], function($, Node) {

	function Tree(tree) {
		this.tree = tree;
		this.nodesInst = [];
		this.renderTree(this.tree, 0);
		this.eventHandlers();
	}

	Tree.prototype = {
		constructor: Node,

		renderTree: function(nodes, level) {
			if (!nodes) return;

			var _this = this;

			level += 1;

			$.each(nodes, function(id, node) {
				var children = false;

				if (node.nodes) { children = true; }

				_this.nodesInst.push(new Node(node.nodeId, node.text, node.parentId, level, children));
				if (node.nodes) {
					return _this.renderTree(node.nodes, level);
				}
			});
		},

		eventHandlers: function() {
			$('#tree').on('click', '.list-group-item', $.proxy(this.onClick, this));
			$('#tree').on('keypress', 'input', $.proxy(this.onEdit, this));
		},

		onClick: function(event) {
			var target = $(event.target);
			var instId = 0;
			var targetInst = 0;
			var classList = target.attr('class') ? target.attr('class').split(' ') : [];

			// Get ID of the node that is taking actions
			if (classList.indexOf('list-group-item') !== -1) {
				instId = target.data('id');
				targetInst = this.nodesInst[instId];
			} else {
				instId = target.parent().data('id');
				targetInst = this.nodesInst[instId];
			}


			// Select Node
			if (classList.indexOf('list-group-item') !== -1) {
				targetInst.selectNode();
			}

			// Collapse Node(s)
			else if (classList.indexOf('glyphicon-chevron-down') !== -1) {
				targetInst.selectNode();
				this.collapseNode(targetInst, true);
				this.forEachChildren(instId, this.collapseNode);
			}

			// Expand Node(s)
			else if (classList.indexOf('glyphicon-chevron-right') !== -1) {
				targetInst.selectNode();
				this.expandNode(targetInst, true);
				this.forEachChildren(instId, this.expandNode);
			}

			// Remove Node(s)
			else if (classList.indexOf('glyphicon-remove') !== -1) {
				this.removeNode(targetInst);
				this.forEachChildren(instId, this.removeNode);
			}

			// Edit Node
			else if (classList.indexOf('glyphicon-pencil') !== -1) {
				targetInst.selectNode();
				targetInst.editNode();
			}

			// Add Node
			else if (classList.indexOf('glyphicon-plus') !== -1) {
				// Getting ID and calculating child level
				var id = $('.list-group-item').length;
				var level = targetInst.level + 1;

				// If node doesn't had childs, add expanded arrow.
				if (!$(targetInst.element).find('.glyphicon-chevron-down').length && !$(targetInst.element).find('.glyphicon-chevron-right').length) {
					$(targetInst.element).prepend('<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>');
				}

				// Create new node
				this.nodesInst.push(new Node(id, 'Inception', targetInst.nodeId, level, false));
			}
		},

		onEdit: function(event) {
			// If return is pressed
			if (event.which == 13) {
				var instId = $(event.target).parent().parent().data('id');
				var targetInst = this.nodesInst[instId];
				targetInst.editNode();
			}
		},

		forEachChildren: function(nodeId, callback) {
			var _this = this;
			var startingNode = nodeId;

			$.each(_this.nodesInst, function(index, node) {
				if (node.parentId == startingNode) {
					callback(node);
					_this.forEachChildren(node.nodeId, callback);
				}
			});
		},

		collapseNode: function(node, parent) {
			node.collapseNode(parent);
		},

		expandNode: function(node, parent) {
			node.expandNode(parent);
		},

		removeNode: function(node) {
			node.removeNode();
		}


	};

	return Tree;

});