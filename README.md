# EG Assignment

## Installation process

Run npm install and grunt to get all dependencies and required files

``npm install && grunt``

Start the project

``npm start``

Follow the white rabbit or Leo if you are Inception fan, don't go too deep tho.

``http://localhost:3001``

## Technology Stack:

1. NodeJS
2. Express.JS
3. Grunt
4. Bootstrap
5. StoreJS
6. jQuery
7. LESS
8. RequireJS

## Wait, what?

You will probably notice that any sort of testing is missing at that point. Unfortunately, lack of experience in that area and limited amount of time played huge role. In general task was quite interesting and I'm glad that I had opportunity to play around with it. Thank you, hope you'll enjoy!

## Changelog (27th of May)

1. Improved file structure
	1. Classes rework
	2. Inheritance improvements
	3. Additional classes
2. New functionality
	1. Expand
	2. Collapse
3. Minor improvements
	1. Animation for Expand/Collapse/Remove


## Have a look

Couple of things inspired me to go little bit deeper and try out same tech-stack in some of mine projects:

https://bitbucket.org/dadecoding/spacegame

Work in progress, but I think it is worth to take a look.