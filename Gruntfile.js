module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
      copy: {
        assets_css_copy: {
          expand: true, flatten: true,
          src: [
            'node_modules/bootstrap/dist/css/bootstrap.min.css',
            'node_modules/bootstrap/dist/css/bootstrap-theme.min.css'
          ],
          dest: 'public/css/'
        },
        assets_js_copy: {
          expand: true, flatten: true,
          src: [
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/jquery/dist/jquery.min.map',
            'node_modules/bootstrap/dist/js/bootstrap.min.js',
            'node_modules/store/store.min.js',
            'node_modules/requirejs/require.js'
          ],
          dest: 'public/js/'
        },
        assets_font_copy: {
          expand: true, flatten: true,
          src: ['node_modules/bootstrap/dist/fonts/**.**'],
          dest: 'public/fonts'
        }
      },
      less: {
        source_css_compile: {
          files: { 'public/css/style.css': 'source/css/style.less' }
        }
      },
      watch: {
        source_css: {
          files: ['source/css/style.less'],
          tasks: ['less:source_css_compile']
        }
      }
  });

  grunt.registerTask('default', ['copy', 'less']);

}
